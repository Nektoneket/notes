from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from notes import views

urlpatterns = [
	#url(r'^$', views.HomeView.as_view(), name='home'),
	url(r'^$', views.NotesListView.as_view(), name='home'),
	url(r'^login/$', views.LoginFormView.as_view(), name='login'),
	url(r'^logout/$', views.LogOutView.as_view(), name='logout'),
	url(r'^sign_up/$', views.RegisterFormView.as_view(), name='sign_up'),
	url(r'^users/$', views.UsersListView.as_view(), name='list_of_users'),
	url(r'^users/(?P<pk>\d+)/$', views.UserProfileView.as_view(), name='user_profile'),
	url(r'^notes/$', views.NotesListView.as_view(), name='list_of_notes'),
	#url(r'^notes/(?P<pk>\d+)/$', views.NoteDetailView.as_view(), name='one_note'),
	url(r'^add_note/$', views.AddNoteFormView.as_view(), name='add_note'),
	url(r'^edit_note/(?P<pk>\d+)/$', views.EditNoteFormView.as_view(), name='edit_note'),
	url(r'^category/(?P<pk>\d+)/$', views.CategoryListOfNotesView.as_view(), name='profile_category'),
	url(r'^add_category/$', views.AddCategoryView.as_view(), name='add_category'),
	url(r'^add_tag/(?P<pk>\d+)/$', views.AddNoteTagView.as_view(), name='add_tag'),
	url(r'^notes_of_tag/(?P<pk>\d+)/$', views.NotesOfTagListView.as_view(), name='notes_of_tag'),
	url(r'^add_subcategory/$', views.AddSubcategoryView.as_view(), name='add_subcategory'),
	url(r'^add_category_to_the_note/(?P<pk>\d+)/$', views.AddNoteCategoryView.as_view(), name='add_note_category'),
	url(r'^delete/(?P<pk>\d+)/$', views.NoteDeleteView.as_view(), name='note_delete'),

] 
