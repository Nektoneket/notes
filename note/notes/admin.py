from django.contrib import admin

from notes.models import Note, Category, Tag
"""
class AuthorAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'email', 'profile_image')
"""

#admin.site.register(Author, AuthorAdmin)
admin.site.register(Note)
admin.site.register(Category)
admin.site.register(Tag)

