# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_title', models.CharField(max_length=100)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('author', models.ForeignKey(related_name='categories', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('heading', models.CharField(max_length=100)),
                ('text', models.TextField()),
                ('media_file', models.FileField(upload_to=b'files/', blank=True)),
                ('image', models.ImageField(upload_to=b'img/', blank=True)),
                ('font_color', models.CharField(default=(b'black', b'ff0000'), max_length=10, blank=True, choices=[(b'red', b'ff0000'), (b'green', b'00ff00'), (b'blue', b'0000ff')])),
                ('font_size', models.IntegerField(default=15, blank=True)),
                ('author', models.ForeignKey(related_name='notes', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag_name', models.CharField(max_length=100)),
                ('author', models.ForeignKey(related_name='tags', to=settings.AUTH_USER_MODEL)),
                ('note', models.ManyToManyField(to='notes.Note')),
            ],
        ),
        migrations.AddField(
            model_name='category',
            name='note',
            field=models.ManyToManyField(to='notes.Note', blank=True),
        ),
        migrations.AddField(
            model_name='category',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='children', blank=True, to='notes.Category', null=True),
        ),
    ]
