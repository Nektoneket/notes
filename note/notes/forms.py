from notes.models import Note, Category, Tag
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.http.request import HttpRequest

class TagForm(forms.Form):
	tag_name = forms.CharField(max_length = 100)


class NoteForm(ModelForm):
	class Meta:
		model = Note		
		fields = ['heading', 'text', 'font_color', 'font_size']


class AddCategoryForm(ModelForm):

	class Meta:
		model = Category
		fields = ['category_title']


class AddSubcategoryForm(forms.Form):
	category = forms.ChoiceField()
	subcategory = forms.CharField(max_length = 100)
	def __init__(self, user, *args, **kwargs):
		super(AddSubcategoryForm, self).__init__(*args, **kwargs)
		self.user = user
		self.fields['category'].choices = [(a,a) for a in Category.objects.filter(author = user)]
		
		
class AddNoteCategoryForm(forms.Form):
	category = forms.ChoiceField()
	def __init__(self, user, *args, **kwargs):
		super(AddNoteCategoryForm, self).__init__(*args, **kwargs)
		self.user = user
		self.fields['category'].choices = [(a,a) for a in Category.objects.filter(author = user)]
	



	

"""
class TagForm(ModelForm):
	
	class Meta:
		model = Tag		
		fields = ['tag_name', 'note']
"""


