from django.shortcuts import render, redirect
from django.views.generic import TemplateView, DetailView, ListView, View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from notes.models import Note, Category, Tag 
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.contrib.auth import login, logout
from notes.forms import NoteForm, TagForm, AddCategoryForm, AddSubcategoryForm, AddNoteCategoryForm
from django.core.urlresolvers import reverse_lazy

class HomeView(TemplateView):
	template_name = 'note/home.html'

	@method_decorator(login_required(login_url='/note/login'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class RegisterFormView(FormView):
	form_class = UserCreationForm
	template_name = 'note/user.html'	
	success_url = '/note'

	def form_valid(self,form):
		form.save()
		return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
	form_class = AuthenticationForm
	template_name = 'note/log_in.html'	
	success_url = '/note'

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)		
		return super(LoginFormView, self).form_valid(form)		

class LogOutView(View):

	def get(self,request,*args,**kwargs):
		logout(self.request)
		return redirect('/note')


class UsersListView(ListView):
	template_name = 'note/get_readers.html'
	model = User

	@method_decorator(login_required(login_url='/note/login'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)

	"""
	def get(self,request,*args,**kwargs):
		
		request.session.set_expiry(60)
		request.session['pause'] = True		
		
		return render(request, self.template_name, {'user_list': User.objects.all()})
	"""	

class UserProfileView(DetailView):
	model = User
	template_name = 'user_profile/profile.html'

	def get_context_data(self, **kwargs):
		user=self.object		
		context = super(UserProfileView,self).get_context_data(**kwargs)

		context['category'] = Category.objects.filter(author = user)	
		return context


	@method_decorator(login_required(login_url='/note/login'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)

class NotesListView(ListView):
	template_name = 'note/home.html'
	model = Note

	@method_decorator(login_required(login_url='/note/login'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)


class AddNoteFormView(FormView):
	form_class = NoteForm
	template_name = 'note/add_note.html'
	success_url = '/note/users/'
	
	def form_valid(self, form):		
		self.object = form.save(commit=False)
		self.object.author = self.request.user
		self.object.save()
		return super(AddNoteFormView, self).form_valid(form)


class EditNoteFormView(UpdateView):
	fields = ('heading','text','image', 'media_file', 'font_color', 'font_size')	
	model = Note
	template_name = 'note/edit_note.html'
	success_url = '/note/users/'



class CategoryListOfNotesView(DetailView):
	template_name = 'user_profile/profile_category.html'
	model = Category

	def get_context_data(self, **kwargs):
		user = self.request.user
		category=self.object
		context = super(CategoryListOfNotesView,self).get_context_data(**kwargs)		
		context['notes'] = self.object.note.all()
		context['category_title'] = category.category_title
		context['category'] = Category.objects.filter(author=user)	
		return context


class AddNoteTagView(FormView):
	form_class = TagForm
	template_name = 'note/add_tag.html'
	success_url = reverse_lazy('list_of_users')
	
	def form_valid(self, form):		
		tag_name = form.cleaned_data['tag_name']
		author = self.request.user
		#print tag_exist, heading
		note_id = str(self.request.path).split('/')
		note = Note.objects.get(id = note_id[3])
		if Tag.objects.filter(tag_name = tag_name).exists():
			Tag.objects.get(tag_name = tag_name).note.add(note)
		else:	
			tag = Tag.objects.create(tag_name = tag_name, author = author)
			tag.note.add(note)
		return super(AddNoteTagView, self).form_valid(form)
	"""
	def form_valid(self, form):		
		self.object = form.save(commit=False)
		self.object.note_id = self.request.session[str(self.request.path)]
		print self.object
		self.object.save()		
		return super(AddNoteTagView, self).form_valid(form)
	"""	

class NotesOfTagListView(DetailView):
	template_name = 'user_profile/profile_tag.html'
	model = Tag

	def get_context_data(self, **kwargs):
		user = self.request.user
		tag = self.object
		context = super(NotesOfTagListView,self).get_context_data(**kwargs)		
		context['category'] = Category.objects.filter(author=user)
		context['notes'] = {tag: tag.note.all()}		
		return context


class AddCategoryView(FormView):
	form_class = AddCategoryForm
	template_name = 'note/add_category.html'
	success_url = '/note/users/'

	def form_valid(self, form, *args, **kwargs):
		
		self.object = form.save(commit = False)
		self.object.author = self.request.user
		self.object.save()
		return super(AddCategoryView, self).form_valid(form)


class AddSubcategoryView(FormView):
	form_class = AddSubcategoryForm
	template_name = 'note/add_subcategory.html'
	success_url = '/note/users/1'

	def get_form_kwargs(self):
		kwargs = super(AddSubcategoryView, self).get_form_kwargs()
		kwargs['user'] = self.request.user
		return kwargs

	def form_valid(self, form, *args, **kwargs):
		category = form.cleaned_data['category']		
		parent_category = Category.objects.get(category_title = category)		
		subcategory = form.cleaned_data['subcategory']		
		author = self.request.user		
		Category.objects.create(
			category_title = subcategory,
			parent = Category.objects.get(category_title = category),
			author = author)
		return super(AddSubcategoryView, self).form_valid(form)


class AddNoteCategoryView(FormView):
	form_class = AddNoteCategoryForm
	template_name = 'note/add_note_category.html'
	success_url = '/note/users/'	

	def get_form_kwargs(self):
		kwargs = super(AddNoteCategoryView, self).get_form_kwargs()
		kwargs['user'] = self.request.user
		return kwargs

	def form_valid(self, form, *args, **kwargs):
		category = form.cleaned_data['category']
		print category
		a = str(self.request.path).split('/')
		note = Note.objects.get(id = a[3])
		print a, note
		aa = Category.objects.get(category_title = category)
		print aa
		Category.objects.get(category_title = category).note.add(note)	
		return super(AddNoteCategoryView, self).form_valid(form)


class NoteDeleteView(DeleteView):
	model = Note
	success_url = '/note/'
	template_name = 'note/note_confirm_delete.html'

	@method_decorator(login_required(login_url='/user/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)





























