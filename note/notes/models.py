from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
import mptt

class Note(models.Model):
	DEFAULT_COLOR = ('black', 'ff0000')
	COLOR_CHOICES = (
		('red', 'ff0000'),
		('green', '00ff00'),
		('blue', '0000ff'),
	)
	author = models.ForeignKey('auth.User', related_name = 'notes')	
	heading = models.CharField(max_length = 100)
	text = models.TextField()
	media_file = models.FileField(upload_to = 'files/', blank = True)
	image = models.ImageField(upload_to = 'img/', blank = True)
	font_color = models.CharField(max_length=10, choices=COLOR_CHOICES, default = DEFAULT_COLOR, blank = True)
	font_size = models.IntegerField(default = 15, blank = True)

	def __unicode__(self):
		return '{0}'.format(self.heading)


class Category(MPTTModel, models.Model):
	category_title = models.CharField(max_length = 100)
	author = models.ForeignKey('auth.User', related_name = 'categories')
	note = models.ManyToManyField(Note, blank = True)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

	class MPTTMeta:
		order_insertion_by = ['category_title']

	def __unicode__(self):
		return '{0}'.format(self.category_title)


class Tag(models.Model):
	tag_name = models.CharField(max_length = 100)
	note = models.ManyToManyField(Note)
	author = models.ForeignKey('auth.User', related_name = 'tags') 
	def __unicode__(self):
		return '{0}'.format(self.tag_name)



