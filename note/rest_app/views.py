from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_app.serializers import UserSerializer, NoteSerializer, CategorySerializer, TagSerializer
from notes.models import Note, Category, Tag
from rest_framework import mixins
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework import permissions, renderers
from rest_app import permissions as custom_permission

class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer 
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)   

    def get_queryset(self):
        queryset = self.queryset
        print self.kwargs
        return queryset.all()
 

class NoteList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class NoteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, custom_permission.IsOwnerOrReadOnly,)


class AllCategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def perform_create(self, serializer):
    	serializer.save(author=self.request.user)


class UserCategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    
    def get_queryset(self):
        queryset = self.queryset
        author = User.objects.get(id = self.kwargs['pk'])
        return queryset.filter(author=author)
    
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, custom_permission.IsOwnerOrReadOnly,)


class TagList(generics.ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class TagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, custom_permission.IsOwnerOrReadOnly,)


class CurrentUserTagList(generics.ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    
    def get_queryset(self):
        queryset = Tag.objects.all()
        return queryset.filter(author=self.request.user)
    
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class UserTagList(generics.ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    
    def get_queryset(self):
        queryset = self.queryset
        author = User.objects.get(id = self.kwargs['pk'])
        return queryset.filter(author=author)
    
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class UserNoteList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def get_queryset(self):
        queryset = self.queryset
        author = User.objects.get(id = self.kwargs['pk'])
        return queryset.filter(author=author)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
 

class CurrentUserNoteList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def get_queryset(self):
        queryset = Note.objects.all()
        
        return queryset.filter(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
 

class TagNotesList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    
    def get_queryset(self):
        tag = Tag.objects.get(pk = self.kwargs['pk'])
        return tag.note.all()
    












