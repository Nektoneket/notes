from rest_framework import serializers
from notes.models import Note, Category, Tag

from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'password', 'id']


class NoteSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    
    class Meta:
        model = Note
        fields = ['heading', 'text', 'author', 'id', 'image', 'media_file', 'font_color', 'font_size']
      


class CategorySerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    #parent = serializers.ChoiceField(choices=, allow_blank = True)
    class Meta:
        model = Category
        fields = ['id', 'author', 'category_title', 'note', 'parent']


class TagSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    
    class Meta:
        model = Tag
        #fields = ['id', 'author', 'category_title', 'note', 'parent']
