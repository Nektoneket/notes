from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from rest_app import views
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>\d+)/$', views.UserDetail.as_view()),

    url(r'^notes/$', views.NoteList.as_view()),
    url(r'^notes/(?P<pk>\d+)/$', views.NoteDetail.as_view()),    
    url(r'^user/notes/$', views.CurrentUserNoteList.as_view()),
    url(r'^user/(?P<pk>\d+)/notes/$', views.UserNoteList.as_view()),
        
    url(r'^categories/$', views.AllCategoryList.as_view()),
    url(r'^categories/(?P<pk>\d+)/$', views.CategoryDetail.as_view()),
    url(r'^user/(?P<pk>\d+)/categories/$', views.UserCategoryList.as_view()),    

    url(r'^tags/$', views.TagList.as_view()),
    url(r'^tags/(?P<pk>\d+)/$', views.TagDetail.as_view()),    
    url(r'^user/tags/$', views.CurrentUserTagList.as_view()),
    url(r'^user/(?P<pk>\d+)/tags/$', views.UserTagList.as_view()),
    url(r'^user/tags/(?P<pk>\d+)/notes$', views.TagNotesList.as_view()),

] 

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

